module Navigate

export main
# using Pkg
# Pkg.add(["LightGraphs", "LinearAlgebra", "GraphPlot", 
# 	 "Compose", "Cairo", "Fontconfig", "Images", "ImageIO" , 
# 	 "ImageCore", "FileIO", "ImageMagick", "FixedPointNumbers",
#          "DataStructures", "HTTP", "System"])

import Cairo
using Compose
using DataStructures
using FileIO
using FixedPointNumbers
using Fontconfig
using GraphPlot
using HTTP
using ImageMagick
using Images, ImageIO , ImageCore
# using LightGraphs
using Graphs
using System



#-----------------------------------------------------------

function calculate_scaling_factor(src::Tuple{Float64,Float64},dst::Tuple{Float64,Float64})
    # x / y
    (src[2]-dst[2])/(src[1]-dst[1]) |> abs |> (k -> round(k, digits = 3))
end

function get_grid_dimension(map_scaling_factor::Float64, grid_scale_multiplier::Int64)
    # x > y
    (Int64(floor(map_scaling_factor * grid_scale_multiplier)), grid_scale_multiplier)
end

#------------------------------------------------------------

function get_coordinate(P::CartesianIndex{2},
                        init_lat_long::Tuple{Float64,Float64},
                        ape::Tuple{Float64, Float64})
    (init_lat_long[1] - ape[1] * (P[1] - 1), init_lat_long[2] + ape[2] * (P[2] - 1))

end

function get_angle_per_edge(x_grid::Int64,
                            y_grid::Int64,
                            initial_::Tuple{Float64, Float64},
                            finial_::Tuple{Float64, Float64})
    x_ = (finial_[2] - initial_[2]) / (x_grid - 1)
    y_ = (initial_[1] - finial_[1]) / (y_grid - 1)
    (y_, x_)
end

function get_angle_per_edge_1(x_grid::Int64,
                              y_grid::Int64,
                            initial_::Tuple{Float64, Float64},
                              finial_::Tuple{Float64, Float64})
    x_ = (finial_[2] - initial_[2]) / (x_grid)
    y_ = (initial_[1] - finial_[1]) / (y_grid)
    (y_, x_)
end

function make_lattitude_longitude_matrix(init_lat_long::Tuple{Float64,Float64},
                                         xx::Int64,
                                         yy::Int64)
    cc_matrix = Array{Tuple{Float64, Float64}}(undef, yy, xx)
    for k in 1:xx, h in 1:yy
        cc_matrix[h,k] = get_coordinate_defaut(CartesianIndex(h,k))
    end
end

#------------------------------------------------------------

function lat_long_2_cartesian(pair::Tuple{Float64, Float64},
                              init_lat_long::Tuple{Float64, Float64},
                              ape::Tuple{Float64, Float64})
    y = (init_lat_long[1] - pair[1])/ape[1] + 1 |> ceil |> Int64
    x = (pair[2] - init_lat_long[2])/ape[2] + 1 |> ceil |> Int64
    CartesianIndex(y,x)
end

function cartesian_pair_2_lat_lon_pair(PointPair::Tuple{CartesianIndex{2},CartesianIndex{2}},
                                       init_lat_long::Tuple{Float64,Float64},
                                       ape::Tuple{Float64, Float64})
    (get_coordinate(PointPair[1], init_lat_long, ape), get_coordinate(PointPair[2], init_lat_long, ape))
end

function edges_2_cartesian_pair(edge::Graphs.SimpleGraphs.SimpleEdge{Int64},xx::Int64)
    (vertex_2_cartesian(src(edge), xx), vertex_2_cartesian(dst(edge), xx))
end

#------------------------------------------------------------

function make_x_y(x::Int, y::Int)
    m = fill(0,x*y)
    n = fill(0,x*y)
    for i ∈ CartesianIndices((y,x))
        m[cartesian_2_vertex(i,x)] = i[2]
        n[cartesian_2_vertex(i,x)] = i[1]
    end
    (m,n)
end

function edge_2_vertex(edg::Graphs.SimpleGraphs.SimpleEdge{Int64})
    [src(edg),dst(edg)]
end

function plot_planar_graph_color_vertices_list(main_graph::SimpleGraph,
                                               fin_path::Array{Graphs.SimpleGraphs.SimpleEdge{Int64},1},
                                               discarded_vertices::Array{Int64,1},
                                               x::Int,
                                               y::Int,
                                               pdf_name::String)

    pp = unique(foldl(append!, edge_2_vertex.(fin_path)))
    dd = discarded_vertices
    
    a = fill(1,y*x)
    for i in 1:x*y
        if i ∈ pp
            a[i] = 2
        elseif i ∈ dd
            a[i] = 3
        end
    end
    # Vertex, path, discarded 
    colors_ = [colorant"white", colorant"red", colorant"black"]
    vertex_colors = colors_[a]
    x__y = make_x_y(x, y)
    draw(PDF(pdf_name, 1cm*x, 1cm*y, dpi = 5),
         gplot(main_graph, x__y[1], x__y[2],
               nodefillc = vertex_colors,nodesize = 3.0,
               edgestrokec = colorant"white", edgelinewidth = 0.5,
               EDGELINEWIDTH = 6.0 / sqrt(nv(main_graph))))
end


function plot_planar_graph_color_from_edge_vector(main_graph::SimpleGraph,
                                                  fin_path::Array{Graphs.SimpleGraphs.SimpleEdge{Int64},1},
                                                  discarded::Array{Graphs.SimpleGraphs.SimpleEdge{Int64},1},
                                                  x::Int,
                                                  y::Int,
                                                  pdf_name::String)

    pp = unique(foldl(append!, edge_2_vertex.(fin_path)))
    dd = unique(foldl(append!, edge_2_vertex.(discarded)))
    
    a = fill(1,y*x)
    for i in 1:x*y
        if i ∈ pp
            a[i] = 2
        elseif i ∈ dd
            a[i] = 3
        end
    end

    colors_ = [colorant"lightgray", colorant"red", colorant"black"]
    vertex_colors = colors_[a]
    x__y = make_x_y(x,y)
    draw(PDF(pdf_name, 1cm*x, 1cm*y, dpi = 5),
         gplot(main_graph, x__y[1], x__y[2],
               nodefillc = vertex_colors,nodesize = 3.0,
               edgestrokec = colorant"lightgray", edgelinewidth = 0.5,
               EDGELINEWIDTH = 6.0 / sqrt(nv(main_graph))))
end

#------------------------------------------------------------

function cartesian_2_vertex(P::CartesianIndex{2}, xx::Int64)
    xx * (P[1] - 1) + P[2]
end

function vertex_2_cartesian(vertex::Int64, xx::Int64)
    m = (Int64(ceil(vertex/xx)))
    n = vertex % xx == 0 ? xx : vertex % xx
    CartesianIndex(m, n)
end

#------------------------------------------------------------

function transform_function(pixel_::RGBA{Normed{UInt8,8}})
    r = Int(reinterpret(red(pixel_)))
    g = Int(reinterpret(green(pixel_)))
    b = Int(reinterpret(blue(pixel_)))
    if r ∈ [204,197,148,203] && g ∈ [220,218,193,220] && b ∈ [170,160,225,169] 
        Gray{N0f8}(0.0)
    else
        Gray{N0f8}(1.0)
    end
end

function transform_function(pixel_::RGB{Normed{UInt8,8}})
    r = Int(reinterpret(red(pixel_)))
    g = Int(reinterpret(green(pixel_)))
    b = Int(reinterpret(blue(pixel_)))
    if r ∈ [204,197,148,203] && g ∈ [220,218,193,220] && b ∈ [170,160,225,169] 
        Gray{N0f8}(0.0)
    else
        Gray{N0f8}(1.0)
    end
end

function image_2_BW(main_image::String, bw_path::String, filter_function::Function)
    oo = load(main_image)
    gray_matix = filter_function.(oo)
    save(bw_path, gray_matix)
    gray_matix
end

#------------------------------------------------------------
# Interms of vertices

function image_2_vertices_array(main_image::String,
                                bw_image::String,
                                xx::Int,
                                yy::Int,
                                full::Bool)
    bw = image_2_BW(main_image, bw_image, transform_function)
    i_r = size(bw)
    pp = i_r[2] / xx
    qq = i_r[1] / yy
    image_cartesian_2_grid_cartesian(a) =
        begin
            CartesianIndex(Int64(ceil(a[1]/qq)), Int64(ceil(a[2]/pp)))
        end    
    vertex_2_edges_array(a) =
        begin
            l = neighbors(grid_graph,a)
            [Edge(a,b) for b in l]
        end
    # grid_graph = grid((xx,yy))
    grid_graph = make_grid_graph(xx, yy; full=full)
    image_cartesian_2_grid_cartesian.(findall((k -> k == Gray{N0f8}(0.0)), bw)) |>
        unique |>
        k -> map(m -> cartesian_2_vertex(m,xx), k) |> unique
end

function remove_edges_from_vertices_array(grid_graph::SimpleGraph{Int64},
                                          vertices_list::Array{Int64,1})
    for i ∈ vertices_list
        while neighbors(grid_graph, i) != []
            for v ∈ neighbors(grid_graph, i)
                 rem_edge!(grid_graph, i, v)
            end
        end
    end
end


#------------------------------------------------------------


function heuristic_fn(vertex::Int64, goal_vertex::Int64, xx::Int64)
    m = vertex_2_cartesian(vertex,xx)
    n = vertex_2_cartesian(goal_vertex,xx)
    √abs((m[2]-n[2])^2 + (m[1]-n[1])^2) * 1.1
end

function get_image(image_path::String,
                   api_::String,
                   s_factor::Float64,
                   initial_::Tuple{Float64, Float64},
                   finial_::Tuple{Float64, Float64},
                   height::Int64)
                                                
    url = "https://maps.geoapify.com/v1/staticmap?"
    style = "klokantech-basic"

    # height = 700
    width = (s_factor * height) |> ceil |> Int64
    
    lat_1 = initial_[1]
    long_1 = initial_[2]

    lat_2 = finial_[1]
    long_2 = finial_[2]
    format_ = "png"

    oo = HTTP.get("$(url)style=$(style)&height=$(height)&width=$(width)&area=rect:$(long_1),$(lat_1),$(long_2),$(lat_2)&format=$(format_)&apiKey=$(api_)")
    write(image_path, oo.body)
end

function make_grid_graph(x::Int, y::Int; full::Bool=true)
    gg = grid((x, y))
    if full == true
        mtx = adjacency_matrix(gg, Float64)
        s = 1.41421
        for i ∈ 1:(x*y - x)
            if i % x == 0
                add_edge!(gg, i, i - 1 + x)
                mtx[i, i - 1 + x] = s
                mtx[i - 1 + x, i] = s
            elseif i % x == 1
                add_edge!(gg, i, i + 1 + x)
                mtx[i + 1 + x, i] = s
                mtx[i, i + 1 + x] = s
            else
                add_edge!(gg, i, i - 1 + x)
                mtx[i, i - 1 + x] = s
                mtx[i - 1 + x, i] = s

                add_edge!(gg, i, i + 1 + x)
                mtx[i + 1 + x, i] = s
                mtx[i, i + 1 + x] = s
            end
        end
        (gg, mtx)
    else
        (gg, weights(gg))
    end
end



# START ------------------------------------------------------------

function main(y_grid::Int64; full::Bool = true)

    #--------------------------------------------------
    # Set the required Positon Coordinates
    #--------------------------------------------------    

    # Source Coordinate

    src_latitude = 12.408906
    src_longitude = 76.579741

    # Distination Coordinate

    dis_latitude = 12.281065
    dis_longitude = 76.276847

    #--------------------------------------------------
    
    if Sys.islinux()
        main_image = "Main.png"
        bw_image = "BW_Main.png"
        pdf_name = "Graph.pdf"
    end
    
    if Sys.iswindows()
        main_image = ".\\Main.png"
        bw_image = ".\\BW_Main.png"
        pdf_name = ".\\Graph.pdf"
    end

    #--------------------------------------------------
    api_ = "1a125ae98970492e9f2373090a30f265"
    #--------------------------------------------------

    src_ = (src_latitude, src_longitude)
    dis_ = (dis_latitude, dis_longitude)
    
    initial_latitude = maximum((src_[1], dis_[1]))
    initial_longitude = minimum((src_[2], dis_[2]))
    
    final_latitude = minimum((src_[1], dis_[1]))
    final_longitude = maximum((src_[2], dis_[2]))
    
    initial_ = (initial_latitude, initial_longitude)
    finial_ = (final_latitude, final_longitude)

    #--------------------------------------------------

    km_per_degree  = 111.19493
    
    s_factor = calculate_scaling_factor(initial_ , finial_)
    y_resolution = 700
    
    x_y = get_grid_dimension(s_factor, y_grid)

    x = x_y[1]
    y = x_y[2]
    
    ape = get_angle_per_edge(x, y, initial_, finial_)

    println("--------------------------------------------------")
    println("--> Getting image :")

    @time get_image(main_image, api_, s_factor, initial_, finial_, y_resolution)

    println("--------------------------------------------------")

    (main_graph, edges_weights) = make_grid_graph(x, y, full = full)

    println("x grid size = $x")
    println("y grid size = $y")

    println("--------------------------------------------------")
    println("--> Calculating vertices Array")
    @time vertices_ = image_2_vertices_array(main_image, bw_image, x, y, full)

    println("--------------------------------------------------")
    println("--> Removing edges")
    @time remove_edges_from_vertices_array(main_graph, vertices_)

    println("--------------------------------------------------")
    println("--> Calculating Path")
    
    start_vertex = lat_long_2_cartesian(src_, initial_, ape) |> k -> cartesian_2_vertex(k, x)
    goal_vertex = lat_long_2_cartesian(dis_, initial_, ape) |> k -> cartesian_2_vertex(k, x)
    
    @time path_ = a_star(main_graph, start_vertex, goal_vertex, edges_weights, n -> heuristic_fn(n, goal_vertex,x))
    
    println("--------------------------------------------------")
    println("--> Creating Graph PDF")
    @time plot_planar_graph_color_vertices_list(main_graph, path_, vertices_, x, y, pdf_name)

    println("--------------------------------------------------")
    println("--> Converting to Cartesianindex pairs")
    @time cartesian_array_of_index = (k -> edges_2_cartesian_pair(k, x)).(path_)

    println("--------------------------------------------------")    
    println("--> Converting to Lat, Long pairs")
    @time lat_long_path = (k -> cartesian_pair_2_lat_lon_pair(k, initial_, ape)).(cartesian_array_of_index)

    println("--------------------------------------------------")    
    println("--> Distance :")
    dist = km_per_degree * (m -> √abs((m[1][1] - m[2][1])^2 + (m[1][2] - m[2][2])^2)).(lat_long_path) |> sum
    println("$dist Km")
    
    println("--------------------------------------------------")    
    println("--> Finial path in terms of Positional Coordinates")
    lat_long_path
    
end

# END ------------------------------------------------------------

end
